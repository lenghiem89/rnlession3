import React from 'react';
import {View} from 'react-native';
import {
  FirstAlert,
  styles,
  Heading,
  ButtonManual,
  Login,
} from './MyComponent/index';

const App = () => {
  // FirstAlert();
  return (
    <View style={styles.all}>
      <Heading title="Yolo System" color="green" />
      <Login />
      <Heading title="Or" color="green" />
      <ButtonManual title="Facebook" color="green" />
      <ButtonManual title="Google" color="red" />
    </View>
  );
};

export default App;
