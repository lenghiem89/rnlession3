import React from 'react';
import {Text, TextInput, TouchableOpacity} from 'react-native';
import styles from './mystyles';
import {RefAttributes} from 'react';
import {
  TextInputProps,
  TextStyle,
  ViewStyle,
  TouchableOpacityProps,
} from 'react-native';

type HeadingProps = {
  title: string;
  color: string;
};
const Heading: React.FC<HeadingProps> = ({title, color}) => {
  return <Text style={[styles.textlarge, {color: color}]}>{title}</Text>;
};

type ButtonProps = {
  title: string;
  color: string;
};
const ButtonManual: React.FC<ButtonProps> = ({title, color}, event) => {
  return (
    <TouchableOpacity
      style={[styles.button, {backgroundColor: color}]}
      onPress={event.preventDefault}>
      <Text style={{fontWeight: 'bold'}}>{title}</Text>
    </TouchableOpacity>
  );
};

export type Props = TouchableOpacityProps & {
  buttonText?: string;
  buttonStyle?: ViewStyle;
  textStyle?: TextStyle;
};

const MyButton: React.FC<Props> = ({
  textStyle = styles.textStyle,
  buttonText = 'Button',
  buttonStyle = styles.button,
  ...others
}) => {
  return (
    <TouchableOpacity style={buttonStyle} {...others}>
      <Text style={textStyle}>{buttonText}</Text>
    </TouchableOpacity>
  );
};
export type Propss = TextInputProps &
  RefAttributes<TextInput> & {
    error?: string;
  };

const MyTextInput: React.FC<Propss> = React.forwardRef<TextInput, Propss>(
  ({error, ...others}: Propss, ref) => {
    return (
      <>
        <TextInput ref={ref} {...others} />
        {error && <Text style={styles.error}>{error}</Text>}
      </>
    );
  },
);

export {Heading, ButtonManual, MyButton, MyTextInput};
