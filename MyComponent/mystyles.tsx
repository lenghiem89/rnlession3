import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  textlarge: {
    backgroundColor: 'white',
    color: 'green',
    fontSize: 50,
    fontWeight: 'bold',
    alignSelf: 'center',
    padding: 0,
  },
  textinput: {
    padding: 12,
    width: '100%',
    color: 'gray',
    borderColor: 'black',
    borderWidth: 0.2,
    borderRadius: 12,
    fontSize: 14,
    marginTop: 5,
    marginBottom: 5,
  },
  all: {
    padding: '3%',
    marginTop: '42%',
    backgroundColor: 'white',
    borderWidth: 0.5,
    borderRadius: 20,
    marginBottom: 0,
    marginLeft: 4,
    marginRight: 4,
    borderColor: 'black',
  },
  button: {
    borderRadius: 24,
    borderColor: 'black',
    borderWidth: 1,
    backgroundColor: 'blue',

    marginTop: 5,
    marginBottom: 0,
    padding: 10,
    width: '100%',
    alignItems: 'center',
  },
  error: {
    color: 'red',
  },

  container: {
    flex: 1,
    margin: 20,
  },
  loginTitle: {
    fontSize: 30,
    textAlign: 'center',
    color: 'green',
    fontWeight: '800',
    marginVertical: 50,
  },
  textInput: {
    height: 50,
    borderColor: 'gray',
    borderRadius: 10,
    borderWidth: 1,
    paddingHorizontal: 10,
  },
  button1: {
    padding: 20,
    marginHorizontal: 10,
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
    backgroundColor: 'blue',
  },
  textStyle: {
    fontWeight: '700',
    textAlign: 'center',
    color: 'black',
  },
});

export default styles;
