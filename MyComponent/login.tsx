import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
  RefAttributes,
} from 'react';
import {
  Alert,
  StatusBar,
  Text,
  TextInput,
  View,
  TouchableOpacityProps,
  TouchableOpacity,
  TextInputProps,
  TextStyle,
  ViewStyle,
} from 'react-native';
import styles from './mystyles';
import {MyButton, MyTextInput} from './component';

export const Login = () => {
  const [userName, setUserName] = useState<string>();
  const [passWord, setPassWord] = useState<string>();
  const [errorUserName, setErrorUserName] = useState<string>();
  const [errorPassWord, setErrorPassWord] = useState<string>();

  const refPassword = useRef<TextInput>(null);

  useEffect(() => {
    Alert.alert('Info', 'Xin chào bạn đến với Yolo System');
  }, []);

  const isValidField = useCallback((value?: string) => {
    return !!value && value.length > 0;
  }, []);

  useEffect(() => {
    if (isValidField(userName)) {
      setErrorUserName(undefined);
    }
  }, [userName, isValidField]);

  useEffect(() => {
    if (isValidField(passWord)) {
      setErrorPassWord(undefined);
    }
  }, [passWord, isValidField]);

  const validateUser = useCallback(() => {
    console.log('validateUser', userName);
    if (isValidField(userName)) {
      setErrorUserName(undefined);
      return true;
    } else {
      setErrorUserName('Bạn phải nhập UserName');
      return false;
    }
  }, [isValidField, userName]);

  const validatePassword = useCallback(() => {
    if (isValidField(passWord)) {
      setErrorPassWord(undefined);
      return true;
    } else {
      setErrorPassWord('Bạn phải nhập password');
      return false;
    }
  }, [isValidField, passWord]);

  const validateLogin = useCallback(() => {
    const isValidUser = validateUser();
    const isValidPassword = validatePassword();
    if (isValidUser && isValidPassword) {
      Alert.alert(
        'Success',
        'Xin chào ' + userName + ', bạn đã đăng nhập thành công vào hệ thống!',
      );
    }
  }, [userName, validatePassword, validateUser]);

  const isValidLogin = useMemo(() => {
    return isValidField(userName) && isValidField(passWord);
  }, [isValidField, passWord, userName]);

  const loginButtonStyle = useMemo(
    () =>
      isValidLogin
        ? styles.button
        : {...styles.button, backgroundColor: 'gray'},
    [isValidLogin],
  );

  const focusPassword = useCallback(() => {
    refPassword?.current?.focus();
  }, []);

  return (
    <View>
      <StatusBar />
      <View>
        <MyTextInput
          error={errorUserName}
          value={userName}
          onChangeText={setUserName}
          style={styles.textInput}
          returnKeyType="next"
          onBlur={validateUser}
          onSubmitEditing={focusPassword}
          clearButtonMode="while-editing"
        />
        <MyTextInput
          error={errorPassWord}
          value={passWord}
          onChangeText={setPassWord}
          secureTextEntry
          style={styles.textInput}
          onBlur={validatePassword}
          ref={refPassword}
          clearButtonMode="while-editing"
          onSubmitEditing={validateLogin}
        />
        <MyButton
          disabled={!isValidLogin}
          style={loginButtonStyle}
          buttonText="Login"
          onPress={validateLogin}
        />
      </View>
    </View>
  );
};
